## PNX_00WW_FIH-user 11 RKQ1.200906.002 00WW_6_190 release-keys
- Manufacturer: hmd global
- Platform: sdm710
- Codename: PNX_sprout
- Brand: Nokia
- Flavor: PNX_00WW_FIH-user
- Release Version: 11
- Id: RKQ1.200906.002
- Incremental: 00WW_6_190
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: 420
- Fingerprint: Nokia/Phoenix_00WW/PNX_sprout:11/RKQ1.200906.002/00WW_6_190:user/release-keys
- OTA version: 
- Branch: PNX_00WW_FIH-user-11-RKQ1.200906.002-00WW_6_190-release-keys
- Repo: nokia_pnx_sprout_dump_22936


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
